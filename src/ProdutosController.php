<?php
namespace Onicmspack\Produtos;

use Illuminate\Http\Request;
use Onicmspack\Produtos\Models\Produto as Produto;
use Onicmspack\Produtos\Models\ProdutoAtributos as ProdutoAtributos;
use Onicmspack\Categorias\Models\Categoria as Categoria;
use Onicmspack\Atributos\Models\Atributo as Atributo;
use Onicmspack\Marcas\Models\Marca as Marca;
use Onicmspack\Produtos\Models\ProdutoFoto as ProdutoFoto;

use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Onicmspack\Produtos\Requests\ProdutosRequest as ProdutosRequest;
use Onicms\Http\Controllers\Controller;

 
class ProdutosController extends Controller
{
    public $caminho = 'admin/produtos/';
    public $views   = 'admin/vendor/produtos/';
    public $titulo  = 'Produtos';    
    public $categorias;
    public $marcas;
    public $atributos;
    public $atributos_array = array();
    public $atributos_string;

    public function inicia_models()
    {
        // Categorias para popular o select em create e edit:
        $this->categorias = ['' => '-- Selecione a categoria --'] + Categoria::orderBy('nome')->lists('nome','id')->toArray();
        $this->marcas = ['' => '-- Selecione a marca --'] + Marca::orderBy('nome')->lists('nome', 'id')->toArray();
        $this->atributos = Atributo::orderBy('nome')->get();
        if(!empty($this->atributos)){
            foreach($this->atributos as $atributo){
                // add na string
                if(!empty($this->atributos_string)) $this->atributos_string .= ',';
                    $this->atributos_string .= $atributo->nome;
                // popula o array:
                $this->atributos_array[$atributo->id] = $atributo->nome;
            }
        }
    }

    
    public function index()
    {
        $registros = Produto::all();
        $registros = configurar_status_toogle($registros, $this->caminho);
        // Configurando os toggles:
        foreach($registros as $registro){
            $registro->html_toggle_destaque = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => $registro->destaque,
                                                    'texto_ativo' => 'Sim',
                                                    'ajax'        => true,
                                                    'ajax_url'    => $this->caminho.$registro->id.'/destaque/atualizar_status/',
                                                    'texto_inativo' => 'Não') );
        }
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        $this->inicia_models();
        $html_toggle = gerar_status_toggle( array('status' => 1) ); 
        $html_destaque_toggle = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => 0,
                                                    'texto_ativo' => 'Sim',
                                                    'texto_inativo' => 'Não') );
        $html_preco_site_toggle          = gerar_status_toggle( array('coluna' => 'exibir_preco_site', 
                                                                      'exibir_preco_site' => 0,
                                                                      'texto_ativo' => 'Sim',
                                                                      'texto_inativo' => 'Não') );
        
        $atributos_valores = array();
        // colocando null para os atributos, ja que é um novo cadastro e não terá nada pra ele:
        foreach($this->atributos as $atributo){
            $atributos_valores[$atributo->id] = null;
        }
        return view($this->views.'.form',[
                    'titulo'        => $this->titulo,
                    'caminho'       => $this->caminho,
                    'html_toggle'   => $html_toggle,
                    'marcas'  => $this->marcas,
                    'categorias'    => $this->categorias,
                    'html_destaque_toggle' => $html_destaque_toggle,
                    'html_preco_site_toggle' => $html_preco_site_toggle,
                    'atributos' => $this->atributos,
                    'atributos_valores' => $atributos_valores,
                    'atributos_string'  => $this->atributos_string,
                    'atributos_array'   => $this->atributos_array,
               ]);
    }

    public function store(ProdutosRequest $request)
    {

        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;

        $c= Produto::create($input);

        // Upload das fotos do ambienete:
        $this->upload_multiplo($input, $c->id);

        $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $this->inicia_models();
        $registro = Produto::find($id);
        
        $html_toggle = gerar_status_toggle( $registro );
        $html_destaque_toggle = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => $registro->destaque,  
                                                    'texto_ativo' => 'Sim',
                                                    'texto_inativo' => 'Não') );
        $html_preco_site_toggle          = gerar_status_toggle( array('coluna' => 'exibir_preco_site', 
                                                                      'exibir_preco_site' => $registro->exibir_preco_site,
                                                                      'texto_ativo' => 'Sim',
                                                                      'texto_inativo' => 'Não') );

         $imagens = Produto::find($id)->fotos;
        //Preenche os valores dos atributos:
        $atributos_valores = array();
        foreach($this->atributos as $atributo){
            $atributos_valores[$atributo->id] = null;
            if($registro->atributos_valores){
                foreach($registro->atributos_valores as $b){
                    if( ($b->atributo_id == $atributo->id) && !empty($b->valor))
                        $atributos_valores[$atributo->id] = $b->valor;
                }
            }
        }

       
        return view($this->views.'.form', compact('registro'),[
                    'titulo'       => $this->titulo,
                    'caminho'      => $this->caminho,
                    'html_toggle'  => $html_toggle,
                    'marcas' => $this->marcas,
                    'categorias'   => $this->categorias,
                    'html_destaque_toggle' => $html_destaque_toggle,
                    'html_preco_site_toggle' => $html_preco_site_toggle,
                    'atributos' => $this->atributos,
                    'imagens'     => $imagens,
                    'atributos_valores' => $atributos_valores,
                    'atributos_string'  => $this->atributos_string,
                    'atributos_array'   => $this->atributos_array,
               ]);
    }

    public function update(ProdutosRequest $request, $id)
    {
        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;

        if(empty($input['marca_id']))
            $input['marca_id'] = NULL;

        if(!isset($input['exibir_preco_site']))
            $input['exibir_preco_site'] = 0;

        if(!isset($input['destaque']))
            $input['destaque'] = 0;

        if(!isset($input['destaque']))
            $input['destaque'] = 0;

        $input['slug'] = str_slug($input['nome'].'-'.$id, '-');

          // formatando os precos caso venham com vírgula:
        $input['preco'] = str_replace(',', '.', $input['preco']);

         // Salva as imagens:
        $this->upload_multiplo($input['fotos'], $id);
        $update = Produto::find($id)->update($input);

        //Salva os atributos:
        $this->salvar_atributos($input, $id);

        // Em caso de novo atributo:
        $this->novo_atributo($input, $id);

        if(isset($input['remover_arquivo'])){
            foreach($input['remover_arquivo'] as $arq)
                $input[$arq] = null;
        }

        // Upload das fotos do ambienete:
        $this->upload_multiplo($input, $id);

        // Remoção de fotos:
        $this->remover_fotos($input);

        $update = Produto::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');






    }

    public function destroy($id)
    {
        Produto::where('id', '=', $id)->delete();
        Produto::find($id)->delete();
        return redirect($this->caminho);
    }

    public function upload_multiplo($input, $produto)
    {
        $fotos = [];
        // Para cada foto, salva no bd:
        if(isset($input['fotos'])){
            foreach($input['fotos'] as $foto){
                if($foto == null)
                    return false;
                $manipulador = new Arquivo;
                $file = $manipulador->add($foto);
                // recortar:
                $manipulador->recortar('produtos_fotos', 'arquivo_id', $file->id);

                $fotos[] = ['arquivo_id'  => $file->id,
                            'produto_id' => $produto,
                            'created_at'  =>date('Y-m-d H:i:s'),
                            'updated_at'  => date('Y-m-d H:i:s'),
                            ];
            }
        }
        if(count($fotos)){
            ProdutoFoto::insert($fotos);
        }
    }

    public function remover_fotos($input)
    {
        if(isset($input['remover_fotos']))
        {
            foreach($input['remover_fotos'] as $foto_id){
                $foto = ProdutoFoto::find($foto_id);
                // deleta da tabela ambientes_fotos:
                $foto->delete();
                // deleta da tabela arquivos:
                Arquivo::find($foto->arquivo_id)->delete();
            }
        }
    }

    public function marcar_capa($foto_id)
    {
        $foto = ProdutoFoto::findOrFail($foto_id);
        // Marca as fotos deste ambiente como false para principal:
        ProdutoFoto::where('produto_id', '=', $foto->produto_id)->update(['principal' => 0]);
        $foto->principal = 1;
        $foto->save();

        \Session::flash('alert-success', 'Foto principal atualizada');
        return redirect($this->caminho.$foto->produto.'');
    }

    // Atualiza um campo boolean de um registro via ajax
    public function atualizar_status($id, $coluna = 'status')
    {
        // Verifica o status atual e dá um update com o novo status:
        $registro = Produto::find($id);
        // Se encontrou o registro:
        if(isset($registro->{$coluna})){
            $novo = !$registro->{$coluna};
            $update = Produto::find($id)->update( array( $coluna =>$novo ) );
            $resposta['success'] = 'success';
            $resposta['status']  = '200';
        }else{
            $resposta['success'] = 'fail';
            $resposta['status']  = '0';
        }
        return \Response::json($resposta);
    }

     public function salvar_atributos($input, $produto_id)
    {
        $this->inicia_models();
        // Deleta os atributos salvos deste produto:
        $registros = ProdutoAtributos::where('produto_id', '=', $produto_id)->get();
        $ids = array();
        foreach($registros as $registro){
            $ids[] = $registro->id;
        }
        if(!empty($ids)){
            ProdutoAtributos::destroy($ids);
        }
        // Salva-os
        foreach($this->atributos as $atributo){
            if(empty($input['attr_'.$atributo->id])){
                    continue;
            }
            $salvar = array();
            $salvar['atributo_id']  = $atributo->id;
            $salvar['produto_id']   = $produto_id;
            $salvar['valor']         = $input['attr_'.$atributo->id];
            ProdutoAtributos::create($salvar);
        }
    }

    public function novo_atributo($input, $produto_id)
    {
        $this->inicia_models();
        if(empty($input['novo_atributo_nome']))
            return FALSE;

        // Verifica se ele já existe:
        $attr = Atributo::where('nome','=',$input['novo_atributo_nome'])->first();
        if(!isset($attr->id)){
            // se não achou, salva o atributo na tabela atributos:
            $attr = array();
            $attr['nome'] = $input['novo_atributo_nome'];
            $attr = Atributo::create($attr);
        }

        // Agora salva a relação do produto com o atributo:
        if(isset($attr->id) && (!empty($input['novo_atributo_valor'])) ){

            // Verifica se já existe para apenas atualizar o valor:
            $query = ProdutoAtributos::where('atributo_id','=',$attr->id)->where('produto_id','=',$produto_id)->first();
            if(isset($query->id)){
                $update          = array();
                $update['valor'] = $input['novo_atributo_valor'];
                ProdutoAtributos::find($query->id)->update($update);
            }else{
                $salvar = array();
                $salvar['atributo_id']  = $attr->id;
                $salvar['produto_id']   = $produto_id;
                $salvar['valor']         = $input['novo_atributo_valor'];
                ProdutoAtributos::create($salvar);
            }
        }
    }

    public function autocomplete_atributos(Request $req)
    {

        $atributo = $req->term;  
        $data=Atributo::where('nome', 'LIKE', '%'.$atributo.'%')
                    ->take(10)
                    ->get();
        
        $results = array();
        foreach ($data as $key => $v) {
            $results[] = ['value'=>$v->nome];
        }
        
        return response()->json($results);
    }
 
}



