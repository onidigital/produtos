<?php

namespace Onicmspack\Produtos;

use Illuminate\Support\ServiceProvider;

class ProdutosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // de onde carregar as views?
        $this->loadViewsFrom(__DIR__.'/views/produtos', 'produtos');

        // Publicando os arquivos:
        // Views
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/admin/vendor'),
        ], 'views');
        
        // Assets css
        $this->publishes([
            __DIR__.'/assets/css' => base_path('resources/assets/css'),
        ], 'css');
        // Assets js
        $this->publishes([
            __DIR__.'/assets/js' => base_path('resources/assets/js'),
        ], 'scripts');
       // Migrations
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/'),
        ], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Onicmspack\Produtos\ProdutosController');
    }
}