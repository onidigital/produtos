<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosFotosTable extends Migration {

	public function up()
	{
		Schema::create('produtos_fotos', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('arquivo_id')->unsigned();
	                $table->foreign('arquivo_id')->references('id')->on('arquivos');
			$table->integer('produto_id')->unsigned();
	                $table->foreign('produto_id')->references('id')->on('produtos');			
			$table->tinyInteger('principal');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('produtos_fotos');
	}
}
