<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosAtributosTable extends Migration {

	public function up()
	{
		Schema::create('produtos_atributos', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('produto_id')->unsigned();
  			$table->foreign('produto_id')->references('id')->on('produtos');
			$table->integer('atributo_id')->unsigned();
  			$table->foreign('atributo_id')->references('id')->on('atributos');
	                $table->string('valor');
		});
	}

	public function down()
	{
		Schema::drop('produtos_atributos');
	}
}
