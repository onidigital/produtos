<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosTable extends Migration {

	public function up()
	{
		Schema::create('produtos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->text('descricao');
			$table->tinyInteger('status');
			$table->decimal('preco', 6,2);

			$table->integer('marca_id')->unsigned();
			$table->foreign('marca_id')->references('id')->on('marcas');

			$table->integer('categoria_id')->unsigned();
			$table->foreign('categoria_id')->references('id')->on('categorias');

			$table->string('sku', 100);
			$table->tinyInteger('exibir_preco_site');
			$table->tinyInteger('destaque');
			$table->string('slug');
			$table->text('keywords');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('produtos');
	}
}
