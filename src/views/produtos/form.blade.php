@extends('layouts.admin')

@section('content')
@if(!isset($registro->id))
{!! Form::open(['url' => $caminho, 'files'=>true]) !!}
@else
{!! Form::model($registro, ['url' => $caminho.$registro->id, 'method'=>'put', 'files'=>true]) !!}
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">{{ $titulo }} <span class="pull-right">{!! $html_toggle !!}</span></h4>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group" >
                            {!! Form::label('destaque', 'Produto em destaque?') !!}
                            {!! $html_destaque_toggle !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group" >
                            {!! Form::label('nome', 'Nome:') !!}
                            {!! Form::text('nome', null, ['class' => 'form-control', 'autofocus'] ) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" >
                            {!! Form::label('sku', 'SKU:') !!}
                            {!! Form::text('sku', null, ['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('categoria_id', 'Categoria:') !!}
                            {!! Form::select('categoria_id', $categorias, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('marca_id', 'Marca:') !!}
                            {!! Form::select('marca_id', $marcas, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group" >
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Form::label('preco', 'Preço do produto (R$) :') !!}
                            {!! Form::number('preco', null, ['class' => 'form-control', 'min'=>0, 'step'=> 'any'] ) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::label('exibir_preco_site', 'Exibir preço no site?') !!}
                            {!! $html_preco_site_toggle !!}
                        </div>
                    </div>
                </div>

                <div class="form-group" >
                    {!! Form::label('keywords', 'Palavras-chave') !!}
                    {!! Form::text('keywords', null, ['class' => 'form-control', 'placeholder'=>'Digite as palavras-chave para este produto, separadas por vírgula'] ) !!}
                </div>
            </div>
        </div>
    </div>
    

</div>

<div class="row">   
    <div class="col-sm-12">
        <div class="card">
            <div class="content">
                <h4 class="title">Descrição/Informações</h4>
                <div class="form-group" >
                    {!! Form::textarea('descricao', null, ['class' => 'form-control editor'] ) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="content">
                <div class="header">
                    <h4 class="title">Atributos</h4>
                </div>
                <div class="content">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Atributo</th>
                                <th>Valor</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($atributos as $atributo)
                            @if(isset($atributos_valores[$atributo->id]))
                            <tr>
                                <td>{{$atributo->nome}}</td>
                                <td>
                                    {!! Form::text('attr_'.$atributo->id, $atributos_valores[$atributo->id], ['class' => 'form-control', 'id' => 'attr_'.$atributo->id] ) !!}
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-danger btn-fill remover-atributo" id="remover-atributo-{{$atributo->nome}}" onclick="$('{{ '#attr_'.$atributo->id }}').val('')" data-toggle="tooltip" title="Remover"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            @endif
                            @empty
                            <tr>
                                <td colspan="3">
                                    Nenhum atributo encontrado
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <hr>
                    <p>Adicionar um atributo</p>
                <!-- 
                    Para o autocomplete:
                    {{ $atributos_string }}
                    ou
                    $atributos_array (id => nome)
                -->
                <div class="form-group">
                    <div class="input-group novo-atributo">
                        <input type="text" name="novo_atributo_nome" id="novo_atributo_nome" class="form-control" placeholder="Atributo">
                        <input type="text" name="novo_atributo_valor" class="form-control" placeholder="Valor">
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-fill"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- form apenas para as imagens: -->
<!-- form apenas para as imagens: -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="content">
                <div class="header">
                    <h4 class="title">Imagens do produto</h4>
                </div>
                <div class="row"> 
                    @if( isset($registro->fotos))
                    <div class="col-sm-12">
                        <label class="well well-sm pull-right">
                            <input type="checkbox" id="gatilho_check" onclick="toggle_check(this.id, 'checkbox_fotos')" class="btn btn-xls" > 
                            Marcar/desmarcar todas as fotos
                        </label>
                    </div>
                    @forelse($registro->fotos as $foto)
                    <div class="col-sm-2">
                        <a href="{{route('getfile', array($foto->arquivo_id, 'grande') )}}" title="clique para ver a foto original" target="_blank">
                            <img src="{{route('getfile', array($foto->arquivo_id, 'thumb') )}}" alt="Imagem do produto" class="img-responsive thumbnail" />
                        </a>
                        <p><label><input type="checkbox" name="remover_fotos[]" class="checkbox_fotos" value="{{ $foto->id }}" > Remover arquivo</label></p>
                        @if($foto->principal)
                        <span class="label label-warning">Foto principal</span>
                        @else
                        <p><label><a href="{{ route('capa_do_produto', $foto->id) }}" class="btn btn-xls btn-sm" > Marcar como principal</a></label></p>
                        @endif
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <p class="help-block">Nenhuma foto encontrada</p>
                    </div>
                    @endforelse

                    @endif
                </div>

                <div class="form-group" >
                    {!! Form::label('fotos[]', 'Upload de imagens:') !!}
                    {!! Form::file('fotos[]', ['multiple'] ) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('scripts-inline')
<script>
    $( "#novo_atributo_nome" ).autocomplete({
        source: "{{ URL::route('autocomplete_atributos') }}",
        minlength:1,
        autoFocus:true
    });
</script>    
@endsection
