<?php

namespace Onicmspack\Produtos\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoAtributos extends Model
{
    //
     public $table = 'produtos_atributos';

    protected $fillable = [
              'produto_id',
              'atributo_id',
              'valor'
            ];

   
}