<?php

namespace Onicmspack\Produtos\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //
    protected $fillable = [
                           'nome',
                           'descricao',
                           'status',
                           'preco',
                           'marca_id',
                           'categoria_id',
                           'sku',
                           'exibir_preco_site',
                           'destaque',
                           'slug',
                           'keywords',

                   
    ];

     public function atributos_valores()
    {
        return $this->hasMany('Onicmspack\Produtos\Models\ProdutoAtributos');
    }

     public function marca()
    {
      return $this->belongsTo('Onicmspack\Marcas\Models\Marca');
    }

    public function categoria()
    {
      return $this->belongsTo('Onicmspack\Categorias\Models\Categoria');
    }



    // Para retornar o arquivo do slide:
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'imagem');
    }

     public function fotos()
    {
      return $this->hasMany('Onicmspack\Produtos\Models\ProdutoFoto')->orderBy('principal', 'desc');
    }
}