<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
    // Menu do slide:
    Route::get('/produtos/autocomplete_atributos', [
    	'as' => 'autocomplete_atributos',
    	'uses' => 'Onicmspack\Produtos\ProdutosController@autocomplete_atributos'
   		]
   	);
    Route::resource('produtos', 'Onicmspack\Produtos\ProdutosController');
    Route::post('produtos/{id}/atualizar_status','Onicmspack\Produtos\ProdutosController@atualizar_status'); // Atualizar Status Ajax

    Route::get( '/produtos/marcar_capa/{foto_id}', [ 
			'as'   => 'capa_do_produto',
			'uses' => 'Onicmspack\Produtos\ProdutosController@marcar_capa'
			]
		);

});