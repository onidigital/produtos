<?php

namespace Onicmspack\Produtos\Requests;

use Onicms\Http\Requests\Request;

class ProdutosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacao = [
                'nome' => 'required',
                'preco' => 'required',
                'marca_id' => 'required',
                'categoria_id' => 'required',
                'keywords' => 'required',
           
        ];
        return $validacao;
    }
}
